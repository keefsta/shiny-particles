  var canvas = document.createElement('canvas');
  canvas.height=document.body.clientHeight-20;
  canvas.width=document.body.clientWidth-20;
  document.body.appendChild(canvas);
  
  var ctx = canvas.getContext('2d');
  ctx.fillStyle="black";
  ctx.fillRect(0,0,canvas.width,canvas.height);
  
  setInterval(draw, 1000/30);
  
  var arrParticles=[];
  var clock=0;
  var drag=0.98;
  var max=50;
  var fLen=250;
  
  function draw(){
    while (arrParticles.length>=max){arrParticles.shift();}
    
    arrParticles.push(new particle());
    ctx.fillStyle="rgba(0,0,0,0.1)";
    
    ctx.fillRect(0,0,canvas.width,canvas.height);
    
    
    for (var i=0;i<arrParticles.length;i++){
      var p=arrParticles[i];
      render(p.x,p.y,p.z,p.width,p.height,p.h,p.clock);
      p.x=p.x+p.u;
      p.y=p.y+p.v;
      p.u=p.u-(Math.sin(clock/20)*0.6);
      //p.v=p.v-(Math.cos(clock/20)*0.3);
     // if (p.x+p.width>=canvas.width||p.x<=0){p.u=p.u*(-1)};
     // if (p.y+p.height>=canvas.height||p.y<=0){p.v=p.v*(-1)};
      if (p.z>250){p.z=0;p.x=0;}
      p.v=p.v*drag;
      p.u=p.u*drag;
      p.v=p.v+0.0;
      p.z=p.z+p.w;
      p.clock++;
      
   }
 clock++;
 }

   function particle(){
    //this.x=canvas.width/2;
    //this.y=canvas.height/2;
    this.x=0;
    this.y=0;
    //this.x=random(0,canvas.width);
    this.y=random(-canvas.height,canvas.height);
    
    this.z=0;
    this.u=random(-5,5);
    this.v=random(-5,5);
    this.w=3;
    //this.u=0;
    this.v=0;
    this.width=random(1,9);
    this.height=random(1,9);
    this.h=Math.floor(random(0,360));
    this.clock=0;
   }
 
   function random(min,max){
     var range=(max-min);
     return min+(range*Math.random());
   }
   function render(x,y,z,w,h,hue,clock){
     /*for (j=0;j<4;j++){
      // w=w*(1+0.005*clock);
      // h=h*(1+0.005*clock);
       ctx.fillStyle = "hsla("+hue+",60%,50%,"+(1-(0.1*j))+")";
       ctx.fillRect (x-(w*0.2*j),y-(h*0.2*j), w*(1+0.4*j),h*(1+0.4*j));
     

     }*/
       var lightness = 20 + (z/250*25);
       var strLightness = lightness +'%';
       var saturation = 45 + (z/250*50);
       var strSaturation = saturation +'%';
       
       ctx.fillStyle = "hsl("+hue+","+strSaturation+","+strLightness+")";
       var projX = fLen/(fLen - z)*x;
       var projY = fLen/(fLen - z)*y;
       var projW = fLen/(fLen - z)*w;
       var projH = fLen/(fLen - z)*h;
       ctx.save();
       ctx.translate(canvas.width/2,canvas.height/2);
       ctx.fillRect (projX,projY,projW,projH);
       ctx.restore();
   }

